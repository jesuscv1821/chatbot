<?php 
namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Currency;
  
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Incoming\Answer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use GuzzleHttp\Client;


class BotManController extends Controller
{
    public $password;
    public $email;
    public $id;
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

        $botman->hears('set default currency', function ($bot) { $this->setDefaultCurrency($bot); });
        $botman->hears('show currencies', function ($bot) { $this->showCurrencies($bot); });
        $botman->hears('commands', function ($bot) { $this->getCommands($bot); });
        $botman->hears('withdraw', function ($bot) { $this->withdraw($bot); });
        $botman->hears('login', function ($bot) { $this->makeLogin($bot); });
        $botman->hears('deposit', function ($bot) { $this->deposit($bot); });
        $botman->hears('balance', function ($bot) { $this->balance($bot); });

        $botman->listen();
    }
  
    /**
     * Place your BotMan logic here.
     */
    public function getCommands($bot)
    {
        $bot->reply(" 
        - login: login into by bot chat <br /> 
        - set default currency: set a default currency <br /> 
        - show currencies: show current currencies from de API <br />
        - deposit: deposit money into your account <br />
        - withdraw: withdraw money from your account <br />
        - balance: show the balance from you account <br />");
    } 
    public function showCurrencies($bot)
    {
        $currencies = Currency::all();
        $str = '';

        foreach ($currencies as $currency)
        {
            $str .= $currency->currency . ' - ' . $currency->rate . '<br />';
        }

        $bot->reply( $str );
    }
    public function makeLogin($bot)
    {
        $bot->ask('What is your email?', function (Answer $emailInput){
            $this->email = $emailInput->getText();
            
            $this->ask('What is your password?', function (Answer $passwordInput){
                $this->password = $passwordInput->getText();

                $request_login = new \Illuminate\Http\Request();
                $request_login->replace(['email' => $this->email, 'password' => $this->password]);
                
                $response_login = app(UserController::class)->authenticate($request_login);
                if ($response_login->status() == 200) 
                {
                    $token = $response_login->original['token'];

                    $user = User::find(1);

                    $this->id = $user->id;
                    $this->say('Welcome ' . $user->name);
                } 
                else
                {
                    $this->say('Email or Password incorrect, try typing "login" again');
                }
            });
        });
    }
    public function setDefaultCurrency($bot)
    {
        $bot->ask('Type the name of the currency', function (Answer $answerInput){
            $currency = Currency::where('currency', '=', $answerInput)->firstOrFail();
            $user = User::find(1);

            $user->currency_id = $currency->id;
            $user->save();
        });
        
    }
    public function deposit($bot)
    {
        $bot->ask('How much do you want to deposit?', function (Answer $amount){
            $float_value = floatval($amount->getText());
            $this->ask('What is the currency you are depositing?', function (Answer $input_currency){
                $user = User::find(1);
                $balance = $user->balance;
                if (!$balance) 
                {
                    $balance = 0.0;
                }
                try {
                    $currency = Currency::where('currency', '=', $input_currency)->firstOrFail(); 
                    $balance += ($currency->rate*$float_value);
                    $user->balance = $balance;
                    $user->save();
                    $this->say('You deposited '. $float_value . ' '  . $input_currency);
                } catch (\Throwable $th) {
                    $this->say('An error ocurred');
                }
            });
        });
    }
    public function balance($bot)
    {
        $user = User::find(1);
        $currency = Currency::find($user->currency_id);
        $balance = $user->balance;

        if ($balance) 
        {
            $bot->reply('Your account has: ' . $balance . ' ' . $currency->currency);
        }
        else 
        { 
            $bot->reply('Your account has: 0 ' . $currency->currency);
        }
    }
    public function withdraw($bot)
    {
        $bot->ask('How much do you want to withdraw?', function (Answer $amount){
            $float_value = floatval($amount->getText());
            $user = User::find(1);
            $balance = $user->balance;
            if (!$balance) 
            {
                $this->say('You can´t withdraw money when you have 0.0 in your account');
            }
            else
            {
                try {
                    $balance += ($currency->rate - $float_value);
                    $user->balance = $balance;
                    $user->save();
                    $this->say('You withdraw '. $amount . ' '  . $input_currency);
                } catch (\Throwable $th) {
                    $this->say('An error ocurred');
                }
            }   
        });
    }
}